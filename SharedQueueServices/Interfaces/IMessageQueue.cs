using System;
using RabbitMQ.Client;

namespace SharedQueueServices.Interfaces
{
    public abstract class IMessageQueue : IDisposable
    {
        public IModel Channel;
        protected abstract void BindQueue(string exchangeName, string routingKey, string queueName);
        protected abstract void DeclareExchange(string exchangeName, string exchangeType);

        public void Dispose()
        {
        }
    }
}