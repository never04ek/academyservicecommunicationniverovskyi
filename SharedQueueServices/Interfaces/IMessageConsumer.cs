using System;
using RabbitMQ.Client.Events;

namespace SharedQueueServices.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        void Connect();
        void SetAcknoledge(ulong deliveryTag, bool processed);
    }
}