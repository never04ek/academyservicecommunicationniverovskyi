using System;

namespace SharedQueueServices.Interfaces
{
    public abstract class IMessageProducerScope : IDisposable
    {
        public IMessageProducer MessageProducer;

        public void Dispose()
        {
        }
    }
}