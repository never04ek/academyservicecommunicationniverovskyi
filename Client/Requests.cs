using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using Models;

namespace Client
{
    public static class Requests
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";


        public static Project GetProject(int id)
        {
            return JsonConvert.DeserializeObject<Project>(Client.DownloadString($"{_path}/Projects/{id}"));
        }

        public static List<Project> GetProjects()
        {
            return JsonConvert.DeserializeObject<List<Project>>(Client.DownloadString($"{_path}/Projects"));
        }

        public static List<Tasks> GetTasks()
        {
            return JsonConvert.DeserializeObject<List<Tasks>>(Client.DownloadString($"{_path}/Tasks"));
        }

        public static Tasks GetTask(int id)
        {
            return JsonConvert.DeserializeObject<Tasks>(Client.DownloadString($"{_path}/Tasks/{id}"));
        }


        public static Team GetTeam(int id)
        {
            return JsonConvert.DeserializeObject<Team>(Client.DownloadString($"{_path}/Teams{id}"));
        }

        public static List<Team> GetTeams()
        {
            return JsonConvert.DeserializeObject<List<Team>>(Client.DownloadString($"{_path}/Teams"));
        }

        public static User GetUser(int id)
        {
            return JsonConvert.DeserializeObject<User>(Client.DownloadString($"{_path}/Users/{id}"));
        }

        public static List<User> GetUsers()
        {
            return JsonConvert.DeserializeObject<List<User>>(Client.DownloadString($"{_path}/Users"));
        }

        public static string Get(string uri)
        {
            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (var response = (HttpWebResponse) request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static string Post(string uri, string data, string contentType, string method = "POST")
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);

            var request = (HttpWebRequest) WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.ContentLength = dataBytes.Length;
            request.ContentType = contentType;
            request.Method = method;

            using (var requestBody = request.GetRequestStream())
            {
                requestBody.Write(dataBytes, 0, dataBytes.Length);
            }

            using (var response = (HttpWebResponse) request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
            {
                return reader.ReadToEnd();
            }
        }

        public static void CopyEntities()
        {
            var projects = JsonConvert.DeserializeObject<List<Project>>(
                Client.DownloadString(" https://bsa2019.azurewebsites.net/api//Projects"));
            var users = JsonConvert.DeserializeObject<List<User>>(
                Client.DownloadString(" https://bsa2019.azurewebsites.net/api//Users"));
            var tasks = JsonConvert.DeserializeObject<List<Tasks>>(
                Client.DownloadString(" https://bsa2019.azurewebsites.net/api//Tasks"));
            var teams = JsonConvert.DeserializeObject<List<Team>>(
                Client.DownloadString(" https://bsa2019.azurewebsites.net/api//Teams"));

            projects.ForEach(project => Post("https://localhost:5001/api/projects",
                JsonConvert.SerializeObject(project),
                "application/json"));
            users.ForEach(project => Post("https://localhost:5001/api/users", JsonConvert.SerializeObject(project),
                "application/json"));
            tasks.ForEach(project => Post("https://localhost:5001/api/tasks", JsonConvert.SerializeObject(project),
                "application/json"));
            teams.ForEach(project => Post("https://localhost:5001/api/teams", JsonConvert.SerializeObject(project),
                "application/json"));
        }
    }
}