﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.AspNetCore.SignalR.Client;

namespace Client
{
    internal class Program
    {
        private static HubConnection connection;

        private static List<string> messagesList = new List<string>();

        private static void Main(string[] args)
        {
            //Set connection
            try
            {
                connection = new HubConnectionBuilder()
                    .WithUrl("http://localhost:5001/hub")
                    .Build();
                connection.On<string>("ReceiveMessage", (message) =>
                {
                    var newMessage = $"{message}";
                    messagesList.Add(newMessage);
                });
                connection.StartAsync();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            connection.InvokeAsync("SendMessage",
                "333");
            
            foreach (var mes in messagesList)
            {
                Console.WriteLine(mes);
            }
        }

        public void ReceiveMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}