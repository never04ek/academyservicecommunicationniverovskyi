using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class UserRepository : IRepository<User>
    {
        private List<User> _entities = new List<User>();


        public UserRepository(List<User> entities = null)
        {
            if (entities != null)
                _entities = entities;
        }

        public List<User> GetAll()
        {
            return _entities;
        }

        public void Add(User entity)
        {
            _entities.Add(entity);
        }

        public void Delete(User entity)
        {
            _entities.Remove(entity);
        }

        public User Get(int id)
        {
            return _entities.FirstOrDefault(entity => entity.Id == id);
        }
        
        public void Update(User entity)
        {
            _entities[_entities.FindIndex(x => x.Id == entity.Id)] = entity;
        }
    }
}