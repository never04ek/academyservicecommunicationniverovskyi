using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class TasksRepository : IRepository<Tasks>
    {
        private List<Tasks> _entities = new List<Tasks>();


        public TasksRepository(List<Tasks> entities = null)
        {
//            if (entities != null)
//                _entities = entities;
        }

        public List<Tasks> GetAll()
        {
            return _entities;
        }

        public void Add(Tasks entity)
        {
            _entities.Add(entity);
        }

        public void Delete(Tasks entity)
        {
            _entities.Remove(entity);
        }

        public Tasks Get(int id)
        {
            return _entities.FirstOrDefault(entity => entity.Id == id);
        }
        
        public void Update(Tasks entity)
        {
            _entities[_entities.FindIndex(x => x.Id == entity.Id)] = entity;
        }
    }
}