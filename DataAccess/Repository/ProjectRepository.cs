using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> _entities = new List<Project>();


        public ProjectRepository(List<Project> entities = null)
        {
            if (entities != null)
                _entities = entities;
        }

        public List<Project> GetAll()
        {
            return _entities;
        }

        public void Add(Project entity)
        {
            _entities.Add(entity);
        }

        public void Delete(Project entity)
        {
            _entities.Remove(entity);
        }

        public Project Get(int id)
        {
            return _entities.FirstOrDefault(entity => entity.Id == id);
        }

        public void Update(Project entity)
        {
            _entities[_entities.FindIndex(x => x.Id == entity.Id)] = entity;
        }
    }
}