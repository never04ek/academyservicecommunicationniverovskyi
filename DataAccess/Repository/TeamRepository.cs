using System.Collections.Generic;
using System.Linq;
using Models;

namespace DataAccess.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private List<Team> _entities = new List<Team>();


        public TeamRepository(List<Team> entities = null)
        {
            if (entities != null)
                _entities = entities;
        }

        public List<Team> GetAll()
        {
            return _entities;
        }

        public void Add(Team entity)
        {
            _entities.Add(entity);
        }

        public void Delete(Team entity)
        {
            _entities.Remove(entity);
        }

        public Team Get(int id)
        {
            return _entities.FirstOrDefault(entity => entity.Id == id);
        }
        
        public void Update(Team entity)
        {
            _entities[_entities.FindIndex(x => x.Id == entity.Id)] = entity;
        }
    }
}