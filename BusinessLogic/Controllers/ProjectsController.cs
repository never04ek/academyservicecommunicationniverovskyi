﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectsService _projectsService;
//        private readonly IQueueService _queueService;


        public ProjectsController(ProjectsService projectsService)
        {
            _projectsService = projectsService;
//            _queueService = queueService;
        }

        // GET api/projects
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get()
        {
//            _queueService.PostValue(DateTime.Now + "\tGet all projects");
            return Ok(_projectsService.GetProjects());
        }

        // GET api/projects/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
//            _queueService.PostValue(DateTime.Now + $"\tGet {id} project");
            return Ok(_projectsService.GetProject(id));
        }

        // POST api/projects
        [HttpPost]
        public IActionResult Post([FromBody] Project value)
        {
//            _queueService.PostValue(DateTime.Now + "\tAdd project");
            _projectsService.AddProject(value);
            return Ok(_projectsService.GetProjects().Count());
        }

        // PUT api/projects/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Project value)
        {
//            _queueService.PostValue(DateTime.Now + $"\tUpdate {id} project");
            _projectsService.UpdateProject(id, value);
            return Ok(value);
        }

        // DELETE api/projects/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
//            _queueService.PostValue(DateTime.Now + $"\tDelete {id} project");
            _projectsService.DeleteProject(id);
            return Ok();
        }
    }
}