using System.Collections.Generic;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    
    public class TasksController : ControllerBase
    {
        List<Tasks> ss = new List<Tasks>();
        private readonly TasksService _tasksService;

        public TasksController(TasksService tasksService)
        {
            _tasksService = tasksService;
        }

        // GET api/tasks
        [HttpGet]
        public ActionResult Get()
        { 
            return Ok(_tasksService.GetTasks());
        }
        

        // GET api/tasks/5
        [HttpGet("{id}")]
        public ActionResult<Tasks> Get(int id)
        {
            return Ok(_tasksService.GetTask(id));
        }

        // POST api/tasks
        [HttpPost]
        public void Post([FromBody] Tasks value)
        {
            _tasksService.AddTask(value);
        }

        // PUT api/tasks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Tasks value)
        {
            _tasksService.UpdateTask(id, value);
        }

        // DELETE api/tasks/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _tasksService.DeleteTask(id);
        }
    }
}