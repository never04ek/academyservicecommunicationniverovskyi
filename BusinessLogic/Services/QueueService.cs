using System;
using System.Text;
using BusinessLogic.Hubs;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedQueueServices.Interfaces;
using SharedQueueServices.Models;

namespace BusinessLogic.Services
{
    public class QueueService : IQueueService
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<MessageHub> _messageHub;

        public QueueService(
            IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IHubContext<MessageHub> messageHub)
        {
            _messageHub = messageHub;
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public void GetValue(object sender, BasicDeliverEventArgs e)
        {
            var value = Encoding.UTF8.GetString(e.Body);
            _messageHub.Clients.All.SendAsync("Get", value);
            _messageConsumerScope.MessageConsumer.SetAcknoledge(e.DeliveryTag, true);
        }

        public bool PostValue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }  
        }
    }

    
}