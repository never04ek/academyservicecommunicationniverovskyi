﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:53353/ChatHub")
                .Build();
            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0,5) * 1000);
                await connection.StartAsync();
            };
            
             connection.InvokeAsync("SendMessage", "542");

        }
    }
}