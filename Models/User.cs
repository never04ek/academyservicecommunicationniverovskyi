using System;
using Newtonsoft.Json;

namespace Models
{
    public class User : Entity
    {
        [JsonProperty("first_name")]public string FirstName { get; set; }
        [JsonProperty("last_name")]public string LastName { get; set; }
        [JsonProperty("email")]public string Email { get; set; }
        [JsonProperty("birthday")]public DateTime Birthday { get; set; }
        [JsonProperty("registered_at")]public DateTime RegisteredAt { get; set; }
        [JsonProperty("team_id")]public int? TeamId { get; set; }

        public override string ToString()
        {
            return
                $"User = {{ id : {Id}, \nFirstName : {FirstName}, LastName : {LastName},\nEmail : {Email},\nBirthday : {Birthday},\nRegisteredAt : {RegisteredAt},\nTeamId : {TeamId}}}";
        }
    }
}