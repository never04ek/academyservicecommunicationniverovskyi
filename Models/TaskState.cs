namespace Models
{
    public enum TaskState 
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}