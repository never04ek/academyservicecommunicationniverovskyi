using Newtonsoft.Json;

namespace Models
{
    public abstract class Entity
    {
        [JsonProperty("id")] public int Id { get; set; }
    }
}