using System;
using Newtonsoft.Json;

namespace Models
{
    
    public class Tasks : Entity
    {
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("description")] public string Description { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")] public DateTime FinishedAt { get; set; }
        [JsonProperty("state")] public TaskState State { get; set; }
        [JsonProperty("project_id")] public int ProjectId { get; set; }
        [JsonProperty("performer_id")] public int PerformerId { get; set; }

        public override string ToString()
        {
            return
                $"Task = {{id : {Id}, \nName : {Name}, \nDescription : {Description},\nCreatedAt : {CreatedAt},\nFinishedAt : {FinishedAt},\nState : {State},\nProjectId : {ProjectId},\nPerformerId : {PerformerId}}}";
        }
    }
}