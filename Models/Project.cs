using System;
using Newtonsoft.Json;

namespace Models
{
    public class Project : Entity
    {
        [JsonProperty("name")] string Name { get; set; }
        [JsonProperty("description")] public string Description { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")] public DateTime Deadline { get; set; }
        [JsonProperty("author_id")] public int AuthorId { get; set; }
        [JsonProperty("team_id")] public int? TeamId { get; set; }

        public override string ToString()
        {
            return
                $"Project = {{id : {Id}, \nName : {Name}, \nDescription : {Description},\nCreatedAt : {CreatedAt},\nDeadline : {Deadline},\nAuthorId : {AuthorId},\nTeamId : {TeamId}}}";
        }
    }
}